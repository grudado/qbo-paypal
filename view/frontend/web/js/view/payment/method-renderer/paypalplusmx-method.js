/*browser:true*/
/*global define*/
define(
        [
            'Magento_Checkout/js/view/payment/default',
            'Magento_Paypal/js/model/iframe',
            'jquery',
            'Magento_Checkout/js/model/quote',
            'mage/storage',
            'Magento_Checkout/js/model/error-processor',
            'Magento_Checkout/js/model/full-screen-loader',
            'Magento_Checkout/js/model/postcode-validator',
            'ppplus'

        ],
        function (Component, iframe, $, quote, storage, errorProcesor, fullScreenLoader, postcodeValidator) {
            'use strict';

            return Component.extend({
                defaults: {
                    template: 'Qbo_PayPalPlusMx/payment/paypalplusmx-form',
                    paymentReady: true
                },
                accessToken: false,
                isPaymentReady: false,
                payerId: false,
                paymentId: false,
                token: false,
                data: false,
                terms: {
                    term: false,
                    monthly_payment: {
                        value: false
                    }
                },
                minimumInstallmentAmount: 5,
                tokenizeServiceUrl: 'paypalplus/payment/cards',
                paymentApiServiceUrl: 'paypalplus/payment',
                errorProcessor: errorProcesor,
                customerData: quote.billingAddress._latestValue,
                placeOrderServiceUrl: "payment-information",
                /**
                 * Wait until "ppplus" div exists and API responds with payment data
                 * @returns {undefined}
                 */
                initialize: function () {

                    fullScreenLoader.startLoader();
                    this.initPayment();
                    this._super();
                    var self = this;
                    var iframeLoaded = setInterval(function ()
                    {
                        if ($('#ppplus').length && self.isPaymentReady)
                        {
                            if (!window.checkoutConfig.payment.paypalPlusIframe.api.isQuoteReady || window.checkoutConfig.payment.paypalPlusIframe.api.error) {
                                $('#iframe-warning').hide();
                                $('#iframe-error').show();
                                $('#continueButton').prop("disabled", true);
                                return false;
                            }
                            self.initializeIframe();
                            fullScreenLoader.stopLoader();
                            clearInterval(iframeLoaded);
                        }
                    }, 1000);
                },
                /**
                 * Initialize PayPal Object Library
                 * @returns {undefined}
                 */
                initializeIframe: function () {
                    //Hide previous error messages
                    $('#iframe-warning').hide();
                    $('#iframe-error-email').hide();

                    //Build Iframe
                    var self = this;
                    var mode = window.checkoutConfig.payment.paypalPlusIframe.config.isSandbox === "1" ? 'sandbox' : 'live';
                    var installmentsActive = window.checkoutConfig.quoteData.base_grand_total > self.minimumInstallmentAmount && Boolean(parseInt(window.checkoutConfig.payment.paypalPlusIframe.config.installments)) ? true : false;
                    var email = quote.guestEmail ? quote.guestEmail : window.checkoutConfig.customerData.email;
                    var customerData = window.checkoutConfig.customerData;
                    var isEmpty = true;
                    for (var i in customerData) {
                        if(customerData.hasOwnProperty(i)) {
                            isEmpty = false;
                        }
                    }
                    var taxVat = '';
                    if(isEmpty){
                        taxVat =  quote.shippingAddress().vatId ? quote.shippingAddress().vatId : storage.get('taxVat');
                    }else{
                        taxVat = customerData.taxvat;
                    }
                    if (!this.validaCpf(taxVat)) {
                        //This will happen if no cpf is provided
                        $('#iframe-warning').hide();
                        $('#iframe-error-cpf').show();
                        $('#continueButton').prop("disabled", true);
                        //Wait for the user to specify an cpf
                        return false;
                    }
                    if (!email && !$("#customer-email").val()) {
                        //This will happen if no shipping address is required and user adds a payment address before entering an email address
                        $('#iframe-warning').hide();
                        $('#iframe-error-email').show();
                        $('#continueButton').prop("disabled", true);
                        //Wait for the user to specify an email address.
                        $("#customer-email").on('focusout', function () {
                            self.initializeIframe();
                        });
                        return false;
                    }
                    /**
                     * Object script included in <head> 
                     * @see di.xml
                     */
                    this.paypalObject = PAYPAL.apps.PPP(
                            {
                                "approvalUrl": window.checkoutConfig.payment.paypalPlusIframe.api.actionUrl,
                                "placeholder": "ppplus",
                                "mode": mode,
                                "buttonLocation": "outside",
                                "preselection": "none",
                                "surcharging": false,
                                "hideAmount": false,
                                disableContinue: "continueButton",
                                enableContinue: "continueButton",
                                "language": "pt_BR",
                                "country": "BR",
                                "disallowRememberedCards": window.checkoutConfig.customerData.id && window.checkoutConfig.payment.paypalPlusIframe.config.save_cards_token ? false : true,
                                "rememberedCards": window.checkoutConfig.payment.paypalPlusIframe.api.card_token ? window.checkoutConfig.payment.paypalPlusIframe.api.card_token : "1",
                                "useraction": "continue",
                                "payerEmail": email ? email : $("#customer-email").val(),
                                "payerPhone": "055" + window.checkoutConfig.payment.paypalPlusIframe.api.shippingData.telephone ? window.checkoutConfig.payment.paypalPlusIframe.api.shippingData.telephone : window.checkoutConfig.payment.paypalPlusIframe.api.billingData.telephone,
                                "payerFirstName": window.checkoutConfig.payment.paypalPlusIframe.api.shippingData.firstname ? window.checkoutConfig.payment.paypalPlusIframe.api.shippingData.firstname : window.checkoutConfig.payment.paypalPlusIframe.api.billingData.firstname,
                                "payerLastName": window.checkoutConfig.payment.paypalPlusIframe.api.shippingData.lastname ? window.checkoutConfig.payment.paypalPlusIframe.api.shippingData.lastname : window.checkoutConfig.payment.paypalPlusIframe.api.billingData.telephone,
                                "payerTaxId": taxVat,
                                "payerTaxIdType": "BR_CPF",
                                "merchantInstallmentSelection": 0,
                                "miniBrowser":false,
                                //Are installmets activated and order total is greater than $500 ? ($500 is because thats the minimum amount allowed by PayPal)
                                "merchantInstallmentSelectionOptional": true,
                                "hideMxDebitCards": false,
                                "iframeHeight": window.checkoutConfig.payment.paypalPlusIframe.config.iframeHeight,
                                /**
                                 * Do stuff after iframe is loaded
                                 * @returns {undefined}
                                 */
                                onLoad: function () {
                                    console.log("Iframe successfully loaded !");
                                },
                                /**
                                 * Continue after payment is verifies (continueButton)
                                 * 
                                 * @param {string} rememberedCards
                                 * @param {string} payerId
                                 * @param {string} token
                                 * @param {string} term
                                 * @returns {}
                                 */
                                onContinue: function (rememberedCardsToken, payerId, token, term) {
                                    $('#continueButton').hide();
                                    $('#payNowButton').show();
                                    var accessToken = window.checkoutConfig.payment.paypalPlusIframe.api.accessToken;
                                    var paymentId = window.checkoutConfig.payment.paypalPlusIframe.api.paymentId;

                                    self.accessToken = accessToken;
                                    self.paymentId = paymentId;
                                    self.payerId = payerId;
                                    //Show Place Order button

                                    var message = {
                                        message: $.mage.__('Payment has been authorized.')
                                    };
                                    self.messageContainer.addSuccessMessage(message);

                                    if (rememberedCardsToken &&
                                            window.checkoutConfig.customerData.id &&
                                            rememberedCardsToken !== window.checkoutConfig.payment.paypalPlusIframe.api.card_token)
                                    {
                                        self.tokenizeCards(rememberedCardsToken);
                                    }

                                    if (typeof term !== 'undefined') {
                                        self.terms = term;
                                    }
                                    //$('#ppplus').hide();

                                    //end aproved card and payment method, run placePendingOrder
                                    self.placePendingOrder();
                                },
                                /**
                                 * Handle iframe error (if payment fails for example)
                                 * 
                                 * @param {type} err
                                 * @returns {undefined}
                                 */
                                onError: function (err) {
                                    var message = {
                                        message: JSON.stringify(err.cause)
                                    };
                                    //Display response error
                                    //that.messageContainer.addErrorMessage(message); 
                                }
                            });
                },
                /**
                 * Call PayPal API to create payment
                 * 
                 * @returns {mage/storage}
                 */
                initPayment: function () {
                    var self = this;
                    return storage.get(
                            self.paymentApiServiceUrl
                            ).fail(
                            function (response) {
                                var payment = JSON.parse(response.responseText);
                                console.log(payment);
                                console.log("Payment Error:" + response);
                                if (payment.reason) {
                                    self.onPaymentError(payment.reason);
                                } else {
                                    self.onPaymentError(null);
                                }
                            }
                    ).done(
                            function (result) {
                                var payment = JSON.parse(result);
                                if (payment.isQuoteReady) {
                                    //Set payment data to window variable
                                    window.checkoutConfig.payment.paypalPlusIframe.api = payment;
                                    self.isPaymentReady = true;
                                } else {
                                    if (payment.reason) {
                                        self.onPaymentError(payment.reason);
                                    } else {
                                        self.onPaymentError(null);
                                    }
                                }
                            }
                    );
                },
                onPaymentError: function (reason) {
                    var iframeErrorElem = '#iframe-error';
                    if (reason) {
                        if (reason === 'payment_not_ready') {
                            iframeErrorElem = '#iframe-error-payment-not-ready';
                        } else {
                            $(iframeErrorElem).html('');
                            $(iframeErrorElem).append('<div><span>' + reason + '</span></div>');
                        }
                    }
                    $(iframeErrorElem).show();
                    $('#iframe-warning').hide();
                    $('#continueButton').prop("disabled", true);
                    fullScreenLoader.stopLoader();
                },
                /**
                 * Handle Continue button, verify shipping address is set
                 * 
                 * @returns {undefined}
                 */
                doContinue: function () {
                    var self = this;
                    if (this.validateAddress() !== false) {
                        self.paypalObject.doContinue();
                    } else {
                        var message = {
                            message: $.mage.__('Please verify shipping address.')
                        };
                        self.messageContainer.addErrorMessage(message);
                    }
                },
                /**
                 * Gather and set payment after payment is authorized.
                 * This data is sent to the Capture methos via ajax.
                 * @see Qbo\PayPalPlusMgetDatax\Model\Payment
                 * 
                 * @returns {array}
                 */
                getData: function () {
                    var data = {
                        'method': this.getCode(),
                        'additional_data': {
                            'access_token': this.accessToken,
                            'payer_id': this.payerId,
                            'payment_id': this.paymentId,
                            'execute_url': window.checkoutConfig.payment.paypalPlusIframe.api ? window.checkoutConfig.payment.paypalPlusIframe.api.executeUrl : "",
                            'handle_pending_payment': window.checkoutConfig.payment.paypalPlusIframe.config.status_pending,
                            'terms': this.terms.term ? this.terms.term : false,
                            'monthly_payment': this.terms.monthly_payment.value ? this.terms.monthly_payment.value : false
                        }
                    };

                    return data;
                },
                paypalObject: {},
                getCode: function () {
                    return 'qbo_paypalplusmx';
                },
                /**
                 * Places order (PayNow Button)
                 */
                placePendingOrder: function () {
                    var self = this;
                    $(document).ajaxError(function (event, request, settings) {
                        var url = settings.url.split("/").pop();
                        if (url === self.placeOrderServiceUrl) {
                            $('#continueButton').show();
                        }
                    });
                    
                    this.placeOrder();
                },
                /**
                 * Save credit card token.
                 * 
                 * @param {type} token
                 * @returns {unresolved}
                 */
                tokenizeCards: function (token) {
                    var self = this;
                    //self.messageContainer = new Messages();
                    var payload = JSON.stringify({
                        token_id: token
                    });
                    return storage.post(
                            this.tokenizeServiceUrl, payload, false
                            ).fail(
                            function (response) {
                                console.log("Failed saving cards:" + token);
                                //self.errorProcessor.process(response, self.messageContainer);
                                message: $.mage.__('An error ocurred while saving card.');
                            }
                    ).done(
                            function (result) {
                                console.log("Saved cards:" + JSON.stringify(result));
                                var message = {
                                    message: $.mage.__('Card successfully saved.')
                                };
                                //TODO: Let or not the user know about saved card before placing order ? Let merchant decide with config ?
                                //self.messageContainer.addSuccessMessage(message);
                            }
                    );
                },
                /**
                 * Validate CPF.
                 *
                 * @returns {Boolean}
                 */
                validaCpf: function (taxVat) {
                var strCPF = taxVat.toString().replace(/\.|-|\//g, '');
                var Soma;
                var Resto;
                Soma = 0;
                //strCPF  = RetiraCaracteresInvalidos(strCPF,11);
                if (strCPF == "00000000000")
                    return false;
                for (i = 1; i <= 9; i++)
                    Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (11 - i);
                Resto = (Soma * 10) % 11;
                if ((Resto == 10) || (Resto == 11))
                    Resto = 0;
                if (Resto != parseInt(strCPF.substring(9, 10)))
                    return false;
                Soma = 0;
                for (i = 1; i <= 10; i++)
                    Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (12 - i);
                Resto = (Soma * 10) % 11;
                if ((Resto == 10) || (Resto == 11))
                    Resto = 0;
                if (Resto != parseInt(strCPF.substring(10, 11)))
                    return false;
                return true;
            },
                /**
                 * Validate shipping address.
                 * 
                 * @returns {Boolean}
                 */
                validateAddress: function () {

                    this.customerData = quote.billingAddress._latestValue;

                    if (typeof this.customerData.city === 'undefined' || this.customerData.city.length === 0) {
                        return false;
                    }

                    if (typeof this.customerData.countryId === 'undefined' || this.customerData.countryId.length === 0) {
                        return false;
                    }

                    if (typeof this.customerData.postcode === 'undefined' || this.customerData.postcode.length === 0 || !postcodeValidator.validate(this.customerData.postcode, "BR")) {
                        return false;
                    }

                    if (typeof this.customerData.street === 'undefined' || this.customerData.street[0].length === 0) {
                        return false;
                    }
                    if (typeof this.customerData.region === 'undefined' || this.customerData.region.length === 0) {
                        return false;
                    }
                    return true;
                }
            });
        }
);




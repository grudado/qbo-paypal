
var config = {
    map: {
        '*': {
            ppplus: "https://www.paypalobjects.com/webstatic/ppplusdcc/ppplusdcc.min.js"
        }
    },
    shim: {
        'ppplus': {
            'deps': [
                'jquery/jquery.cookie'
            ]
        }
    },
    config: {
        mixins: {
            'Magento_Checkout/js/model/payment-service': {
                'Qbo_PayPalPlusMx/js/plugin/payment-service': true
            }
        }
    }
};
